
#include "Console.hpp"
#include "Ports.hpp"

Console::Console (void)
    : m_termBuff   { reinterpret_cast<Console::Cell *> (0xB8000ULL) }
    , m_currCol    { 0 }
    , m_currRow    { 0 }
    , m_currColors { UInt8 (Color::WHITE | Color::BLACK << 4) }
{ }

Console::Cell & Console::cell (const UInt32 row, const UInt32 col) {
    return m_termBuff [col + NB_COLS * row];
}

void Console::clearRow (const UInt32 row) {
    for (UInt32 col { 0 }; col < NB_COLS; ++col) {
        cell (row, col) = { ' ', m_currColors };
    }
}

void Console::clearScreen (void) {
    for (UInt32 row { 0 }; row < NB_ROWS; ++row) {
        clearRow (row);
    }
    m_currCol = 0;
    m_currRow = 0;
    setCursorPos (m_currCol, m_currRow);
}

Console & Console::instance (void) {
    static Console ret { };
    return ret;
}

void Console::setColors (const Console::Color fg, const Console::Color bg) {
    m_currColors = UInt8 (fg | (bg << 4));
}

void Console::setCursorPos (const UInt32 col, const UInt32 row) const {
    const UInt16 pos { UInt16 (row * NB_COLS + col +1) };
    Ports::outB (0x3D4, 0x0F);
    Ports::outB (0x3D5, UInt8 (pos & 0xFF));
    Ports::outB (0x3D4, 0x0E);
    Ports::outB (0x3D5, UInt8 ((pos >> 8) & 0xFF));
}

void Console::printNewLine (void) {
    m_currCol = 0;
    if (m_currRow < NB_ROWS -1) {
        ++m_currRow;
        setCursorPos (m_currCol, m_currRow);
    }
    else {
        for (UInt32 row { 1 }; row < NB_ROWS; ++row) {
            for (UInt32 col { 0 }; col < NB_COLS; ++col) {
                cell (row -1, col) = cell (row, col);
            }
        }
        clearRow (NB_ROWS -1);
    }
}

void Console::printChar (const char chr) {
    if (chr == '\n' || chr == '\r') {
        printNewLine ();
    }
    else {
        if (m_currCol >= NB_COLS) {
            printNewLine ();
        }
        cell (m_currRow, m_currCol) = { UInt8 (chr), m_currColors };
        ++m_currCol;
        setCursorPos (m_currCol, m_currRow);
    }
}

void Console::printStr (const char * str) {
    for (UInt32 offset { 0 }; str [offset] != '\0'; ++offset) {
        printChar (str [offset]);
    }
}
