
#include "Console.hpp"

extern "C" {

void kernel_main (void) {
    CONSOLE.setColors    (Console::YELLOW, Console::BROWN);
    CONSOLE.clearScreen  ();
    CONSOLE.printStr     ("Welcome in NuOS !");
    CONSOLE.printNewLine ();
    CONSOLE.printNewLine ();
    CONSOLE.printChar    ('#');
}

}
