#ifndef CONSOLE_HPP
#define CONSOLE_HPP

#include "Defs.hpp"

class Console {
    struct Cell {
        UInt8 chr;
        UInt8 colors;
    } * m_termBuff;

    UInt32 m_currCol;
    UInt32 m_currRow;
    UInt8  m_currColors;

    inline Cell & cell (const UInt32 row, const UInt32 col);

    explicit Console (void);

public:
    static const UInt32 NB_COLS { 80 };
    static const UInt32 NB_ROWS { 25 };

    static Console & instance (void);

    enum Color : UInt8 {
        BLACK       =  0,
        BLUE        =  1,
        GREEN       =  2,
        CYAN        =  3,
        RED         =  4,
        MAGENTA     =  5,
        BROWN       =  6,
        LIGHT_GRAY  =  7,
        DARK_GRAY   =  8,
        LIGHT_BLUE  =  9,
        LIGHT_GREEN = 10,
        LIGHT_CYAN  = 11,
        LIGHT_RED   = 12,
        PINK        = 13,
        YELLOW      = 14,
        WHITE       = 15,
    };

    void setColors    (const Color fg, const Color bg);
    void setCursorPos (const UInt32 col, const UInt32 row) const;

    void clearRow    (const UInt32 row);
    void clearScreen (void);

    void printNewLine (void);
    void printChar    (const char chr);
    void printStr     (const char * str);
};

#define CONSOLE Console::instance ()

#endif
