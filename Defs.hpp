#ifndef DEFS_HPP
#define DEFS_HPP

using UInt8  = unsigned char;
using UInt16 = unsigned short;
using UInt32 = unsigned int;
using UInt64 = unsigned long long;

using Int8  = signed char;
using Int16 = signed short;
using Int32 = signed int;
using Int64 = signed long long;

#endif
