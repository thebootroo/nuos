
#include "Ports.hpp"

void Ports::outB (const UInt16 port, const UInt8 val) {
    asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
}

void Ports::outW (const UInt16 port, const UInt16 val) {
    asm volatile ( "outw %0, %1" : : "a"(val), "Nd"(port) );
}

void Ports::outL (const UInt16 port, const UInt32 val) {
    asm volatile ( "outl %0, %1" : : "a"(val), "Nd"(port) );
}

UInt8 Ports::inB (const UInt16 port) {
    UInt8 ret { 0 };
    asm volatile ( "inb %1, %0" : "=a"(ret) : "Nd"(port) );
    return ret;
}

UInt16 Ports::inW (const UInt16 port) {
    UInt16 ret { 0 };
    asm volatile ( "inw %1, %0" : "=a"(ret) : "Nd"(port) );
    return ret;
}

UInt32 Ports::inL (const UInt16 port) {
    UInt32 ret { 0 };
    asm volatile ( "inl %1, %0" : "=a"(ret) : "Nd"(port) );
    return ret;
}
