#ifndef PORTS_HPP
#define PORTS_HPP

#include "Defs.hpp"

class Ports {
public:
    static void outB (const UInt16 port, const UInt8  val);
    static void outW (const UInt16 port, const UInt16 val);
    static void outL (const UInt16 port, const UInt32 val);

    static UInt8  inB (const UInt16 port);
    static UInt16 inW (const UInt16 port);
    static UInt32 inL (const UInt16 port);
};

#endif // PORTS_HPP
