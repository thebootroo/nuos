import qbs;
import qbs.TextFile;
import qbs.Process;

Project {
    name: "NuOS";

    Product {
        name: "nuos";
        type: "application";

        Group {
            name: "ASM";
            files: ["**/*.asm"];
            fileTags: "asm";
        }
        Group {
            name: "C++ Sources";
            files: ["**/*.cpp"];
            fileTags: "cpp";
        }
        Group {
            name: "C++ Headers";
            files: ["**/*.hpp"];
            fileTags: "hpp";
        }
        Group {
            name: "Linker";
            files: ["**/*.ld"];
            fileTags: "ld";
        }
        Group {
            name: "Config";
            files: ["**/*.cfg"]
            fileTags: "cfg";
        }
        Rule {
            multiplex: false;
            inputs: ["asm"];
            prepare: {
                var args = [
                            "-f",
                            "elf64",
                            input.filePath,
                            "-o",
                            output.filePath,
                        ];
                var cmd = new Command ("nasm", args);
                cmd.description = ("compiling " + input.fileName);
                return [cmd];
            }

            Artifact {
                filePath: input.fileName.replace (".asm", ".o");
                fileTags: ["obj"];
            }
        }
        Rule {
            multiplex: false;
            inputs: ["cpp"];
            prepare: {
                var args = [
                            "-c",
                            "-m64",
                            "-I",
                            product.sourceDirectory,
                            "-ffreestanding",
                            "-fno-threadsafe-statics",
                            "-nostdlib",
                            input.filePath,
                            "-o",
                            output.filePath,
                        ];
                var cmd = new Command ("gcc", args);
                cmd.description = ("compiling " + input.fileName);
                return [cmd];
            }

            Artifact {
                filePath: input.fileName.replace (".cpp", ".o");
                fileTags: ["obj"];
            }
        }
        Rule {
            multiplex: true;
            inputs: ["ld", "obj"];
            prepare: {
                var args = [
                            "-n",
                            "-o",
                            output.filePath,
                            "-T",
                            inputs ["ld"][0].filePath,
                        ];
                inputs ["obj"].forEach (function (item) {
                    args.push (item.filePath);
                });
                var cmd = new Command ("ld", args);
                cmd.description = ("linking " + output.fileName);
                return [cmd];
            }

            Artifact {
                filePath: "dist/boot/nuos.bin";
                fileTags: ["bin"];
            }
        }
        Rule {
            inputs: ["cfg"];
            prepare: {
                var args = [
                            input.filePath,
                            output.filePath,
                        ];
                var cmd = new Command ("cp", args);
                cmd.description = ("copying " + output.fileName);
                return [cmd];
            }

            Artifact {
                filePath: ("dist/boot/grub/" + input.fileName);
                fileTags: ["dist"];
            }
        }
        Rule {
            multiplex: true;
            inputs: ["dist", "bin"];
            prepare: {
                var args = [
                            "/usr/lib/grub/i386-pc",
                            "-o",
                            output.filePath,
                            (product.buildDirectory + "/dist"),
                        ];
                var cmd = new Command ("grub-mkrescue", args);
                cmd.description = ("generating " + output.fileName);
                cmd.stdoutFilterFunction = function (line) { return ""; }
                cmd.stderrFilterFunction = function (line) { return ""; }
                return [cmd];
            }

            Artifact {
                filePath: "output/nuos.iso";
                fileTags: ["iso"];
            }
        }
        Rule {
            inputs: ["iso"];
            prepare: {
                var cmd = new JavaScriptCommand;
                cmd.description = ("scripting " + output.fileName);
                cmd.sourceCode = function () {
                    var txt = new TextFile (output.filePath, TextFile.WriteOnly);
                    txt.truncate ();
                    txt.writeLine ("#!/bin/bash");
                    txt.writeLine ("qemu-system-x86_64 -cdrom $(dirname $0)/nuos.iso");
                    txt.close ();
                    var proc = new Process;
                    proc.exec ("chmod", ["a+x", output.filePath]);
                };
                return [cmd];
            }

            Artifact {
                filePath: "output/nuos.sh";
                fileTags: ["application"];
            }
        }
        Group {
            fileTagsFilter: ["iso","application"];
            qbs.install: true;
            qbs.installPrefix: "/";
        }
    }
}
